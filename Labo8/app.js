const mysql = require('mysql');
const express = require('express');
const redirect = require('express-redirect');
const bodyParser = require('body-parser');
const session = require('express-session');
const passwordHash = require('password-hash'); 
const app = express();

app.use(bodyParser.urlencoded({ extended : true }));
app.use(bodyParser.json());
app.use(session ({
      secret: "secret",
      saveUninitialized: false,
      resave: true,
      rolling: true,
      cookie: { maxAge: 500 * 1000 }
   })
);

var currentUser = {
    firstName : "invité",
    lastName : "",
    address: "",
    city: "",
    autorityLevel: 0,
    username: "",
    password: "",
    classes: "",
};

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "ecole"
});

con.connect(function(err) {
  if (err) throw err;
  
});             

redirect(app);
app.set('view engine', 'ejs');

app.use("/public",express.static(__dirname + "/public"));
app.use("/node_modules",express.static(__dirname + "/node_modules"));


app.redirect('/', '/login');

app.get('/login', function(req, res) {
    var sessData = req.session;
    sessData.currentUser = currentUser;
   res.render('pages/login.ejs', {errorCode : 0});
});

app.post('/login', function(req, res) {
    //Verify with query and dehash the password
    var querystring = "SELECT * FROM userdata WHERE username = '" + req.body.username + "';";
  
    con.query(querystring, function (err, result) {
    if (err) throw err;
    
    
    if(result.length > 0) 
    {
        if(passwordHash.verify(req.body.password, result[0].password)) //password is valid
        {
            req.session.currentUser = result[0];
            res.redirect(301, '/home');
        }     
    }
    else  
    {
        res.render('pages/login.ejs', {errorCode : 1});    
    }
  
  });
    
});

app.get('/register', function(req, res) {
   res.render('pages/register.ejs', {errorCode : 0});
});

app.post('/register', function(req, res) {
    
    var querystring = "SELECT * FROM userdata WHERE username = '" + req.body.username + "';";
    var userSetVerif;
    
    con.query(querystring, function (err, result) {
    if (err) throw err;
    userSetVerif = result;
    
    if(req.body.password != req.body.rep_password)
    {
        res.render('pages/register.ejs', {errorCode : 1});
    }
    else if(userSetVerif.length > 0)
    {
        res.render('pages/register.ejs', {errorCode : 2});
    }
    else
    {
        res.render('pages/register_done.ejs');
    
        var autorityType;
    
        if(req.body.type == 'Utilisateur')
            autorityType = '1';
        else
            autorityType = '0';
    
        querystring = "INSERT INTO userdata" +
                      "(firstName, lastName, address, city, autorityLevel, password, username) VALUES " +
                      "('" + req.body.fname + "', '" + req.body.lname + "', '" + req.body.address + "', '" + req.body.city + "', " + autorityType + ", '" + passwordHash.generate(req.body.password) + "', '" + req.body.username + "');";
                      
        con.query(querystring, function (err) {
        if (err) throw err;
        });    
    }
    
    });

});

app.get('/register_done', function(req, res) {
   res.render('pages/register_done.ejs');
});

app.get('/home', function(req, res) {
    
    var userdata = req.session.currentUser;
    req.session= req.session;

    res.render('pages/home.ejs', {currentUser : userdata, accessLevel : userdata.autorityLevel});
});

app.get('/logout', function(req, res){
res.redirect(301, '/login');

});

app.get('/account', function(req, res){
    
    var userdata = req.session.currentUser;
    req.session = req.session;
    
    if(userdata.autorityLevel < 1)
        res.redirect(301, '/home');
    
    var querystring = "SELECT className FROM classes WHERE ";
    var numStr = "";
    
    for (var i=0; i < userdata.classes.length; i++) { 
        if(userdata.classes.charAt(i) != ';')  
            numStr += userdata.classes.charAt(i);
        else
        {
            querystring += "ID = " + numStr;
            numStr = "";
            
            if((i + 1) == userdata.classes.length)
                querystring += ";";
            else
                querystring += " OR ";
        }
    }
     
    if(userdata.classes.length > 0)
    {  
        con.query(querystring, function (err, result) {
        if (err) throw err;
    
        res.render('pages/account.ejs', {currentUser : userdata, classesList : result, accessLevel : userdata.autorityLevel});
    
        });
    }
    else
    {
        res.render('pages/account.ejs', {currentUser : userdata, classesList : [], accessLevel : userdata.autorityLevel}); 
    }   
});

app.get('/users', function(req, res){
    
    var userdata = req.session.currentUser;
    req.session = req.session;
    
    if(userdata.autorityLevel < 2)
        res.redirect(301, '/home');
        
    var querystring = "SELECT * FROM userdata";
    
    con.query(querystring, function (err, result) {
        if (err) throw err;
    
        res.render('pages/users.ejs', {currentUser : userdata, accessLevel : userdata.autorityLevel, userTable : result});
    });   
});

app.post('/users', function (req, res) {
    
    req.session = req.session;
    var querystring = "";
  
    if(req.body.redirectCommand.substring(0, 4) == 'dele')
    {
        
        req.session.usernameTarget  = req.body.redirectCommand.substring(8, 12);
        
        querystring = "DELETE FROM userdata WHERE username = '" + req.session.usernameTarget + "';" ;
        
        con.query(querystring, function (err) {
        if (err) throw err;
        
            res.redirect(301, '/users');
        
        });   
    }
    else if (req.body.redirectCommand.substring(0, 4) == 'edit')
    {
        req.session.usernameTarget = req.body.redirectCommand.substring(8, 12);
        
        res.redirect(301, '/newUser');
        
    }
    else if(req.body.redirectCommand.substring(0, 4) == 'pass')
    {
        req.session.usernameTarget = req.body.redirectCommand.substring(8, 12);
        
        res.redirect(301, '/newPassword');   
    }
    else if(req.body.redirectCommand.substring(0, 3) == 'add')
    {
        req.session.usernameTarget = "";
        
        res.redirect(301, '/newUser');
    }  
});

app.get('/newUser', function(req, res) {
    
    var userdata = req.session.currentUser;
    var usernameTarget = req.session.usernameTarget;
    var errorCode = (req.session.errorCode === undefined)? 0 : req.session.errorCode;
    req.session = req.session;
    
    if(userdata.autorityLevel < 2)
        res.redirect(301, '/home');
        
    var querystring = "SELECT * FROM userdata WHERE username = '" + usernameTarget + "';";
    
     con.query(querystring, function (err, result) {
    if (err) throw err;
        
        querystring = "SELECT * FROM classes;";
        
         con.query(querystring, function (err, classes) {
            if (err) throw err;
        
            res.render('pages/newUser.ejs',{accessLevel : userdata.autorityLevel, usernameTarget : usernameTarget, presetUser : result, classes : classes, errorCode : errorCode});
        });
    });
   
});

app.post('/newUser', function(req, res) {
    
    var querystring;
    var usernameTarget = req.session.usernameTarget;
    req.session = req.session;
    req.session.errorCode = 0;
    
    var autorityType;
    
    if(req.body.type == 'Administrateur')
        autorityType = '2';
    else if(req.body.type == 'Utilisateur')
        autorityType = '1';
    else
        autorityType = '0';
    
    if(usernameTarget !== "")
    {
        //Modify existing user
        querystring = "UPDATE userdata SET " +
                      "firstName = '" + req.body.fname + "'," +
                      "lastName = '" + req.body.lname + "'," +
                      "address = '" + req.body.address + "'," +
                      "city = '" + req.body.city + "'," +
                      "autorityLevel = " + autorityType + "," +
                      "address = '" + req.body.address + "'," +
                      "username = '" + req.body.username + "'," +
                      "classes = '" + req.body.classeList + "' " +
                      "WHERE username = '" + usernameTarget + "';";
                      
        con.query(querystring, function (err) {
        if (err) throw err;
         
         res.redirect(301, '/users');
        
        });
    }
    else
    {
        //Check if username exists + error managing + creation
        var userSetVerif;
        querystring = "SELECT * FROM userdata WHERE username = '" + req.body.username + "';";
        
        con.query(querystring, function (err, result) {
            if (err) throw err;
                  userSetVerif = result;
            consle.log(result);
             if(userSetVerif.length > 0)
            {
                  req.session.errorCode = 2;
                  res.redirect(301, '/newUser');
            }
            else
            {
                  querystring = "INSERT INTO userdata" +
                      "(firstName, lastName, address, city, autorityLevel, password, username, classes) VALUES " +
                      "('" + req.body.fname + "', '" + req.body.lname + "', '" + req.body.address + "', '" + req.body.city + "', " + autorityType + ", '" + passwordHash.generate(req.body.password) + "', '" + req.body.username + "', '" + req.body.SelectedValues + "');";
                  
                  con.query(querystring, function (err) {
                        
                        if (err) throw err;
                        
                        res.redirect(301, '/users');
                  });
                  
                  
            }
        });   
     }  
});

app.get('/newPassword', function(req, res) {
    
    var userdata = req.session.currentUser;
    var usernameTarget = req.session.usernameTarget;
    req.session = req.session;
    
    if(userdata.autorityLevel < 2)
        res.redirect(301, '/home');

    res.render('pages/newPassword.ejs', {accessLevel : userdata.autorityLevel, usernameTarget : usernameTarget});   
});

app.post('/newPassword', function(req, res) {
    
    var usernameTarget = req.session.usernameTarget;
    req.session = req.session;
    
    var querystring = "UPDATE userdata SET password = '" + passwordHash.generate(req.body.newPassword) + "' WHERE username = '" + usernameTarget  + "';";
    
    con.query(querystring, function (err) {
    if (err) throw err;
        
        res.redirect(301, '/users');
    });
});

app.get('/expired', function(req, res) {
   res.render('pages/expired.ejs');
   
   req.session.destroy();
});

app.use(function(req, res){
      
  req.session = req.session;
  
  res.status(404);
  res.render('pages/error_404.ejs');
});


app.listen(8080);


