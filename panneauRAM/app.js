Array.prototype.last = function() {
    return this[this.length-1];
}

const fs = require('fs');
const extfs = require('extfs');
const async = require('async');
const express = require('express');
const session = require('express-session');
const redirect = require('express-redirect');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const bcrypt = require('bcrypt');
const app = express();
const server = app.listen(8080);
const io = require('socket.io').listen(server);
const ON_DEATH = require('death');
const mysql = require('mysql');
const redis = require('redis');
const redisStore = require('connect-redis')(session);
const mqtt = require('mqtt');
const winston = require('winston');
const ip = require('ip');

const users_data_db = 'panel_users';
const users_data_tab = 'users_data';
const saltRounds = 10;
const logDir = './logs/';
const serverInfoDir = __dirname + '/server_info/';
const cmdlog_filename = logDir + 'commands.log';
const alarmlog_filename = logDir + 'alarms.log';
const serverInfo_filename = serverInfoDir + 'info.json';
//const serverSpecs_filename = serverInfoDir + 'specs.json';
const serverPorts_filename = serverInfoDir + 'ports.json';

const serverInfo = JSON.parse(fs.readFileSync(serverInfo_filename));
serverInfo.IPv4 = ip.address();
//const serverSpecs = JSON.parse(fs.readFileSync(serverSpecs_filename));
const serverPorts = JSON.parse(fs.readFileSync(serverPorts_filename));


const logger = new winston.Logger({
    transports: [
        new (winston.transports.File)({
            level : 'info',
            name : 'cmdlog_file',
            filename : cmdlog_filename,
            timestamp : true,
            json : true
        }),
        new (winston.transports.Console)({
            name : 'cmdlog_console',
            level : 'info',
            colorized: true
        }),
        new (winston.transports.File)({
            level : 'warn',
            name : 'alarmlog_file',
            filename : alarmlog_filename,
            timestamp : true,
            json : true
        }),
        new (winston.transports.Console)({
            level : 'warn',
            name : 'alarmlog_console',
            colorized: true
        })
    ]
});


var redisClient = redis.createClient();
var mqttClient = mqtt.connect({
    port: serverPorts.mqtt,
    host: 'localhost'
 });

//Backup solution in case of session bug
//var userKey = false;

const pool = mysql.createPool({
      connectionLimit: 50,
      host: 'localhost',
      user: 'root',
      password: 'root',
      database: users_data_db,
      debug: false
});

app.set('view engine', 'ejs');
app.use("/public",express.static(__dirname + "/public"));
app.use("/node_modules",express.static(__dirname + "/node_modules"));
app.use("/modules",express.static(__dirname + "/modules"));
app.set('views', __dirname + '/views');

app.use( session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false,
  store: new redisStore({
    host: 'localhost',
    port: serverPorts.redis,
    client: redisClient,
    ttl: 60 * 1000
  })
}));


redirect(app);
app.use(cookieParser("secret"));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var activeAlarms = [];
var mqttChannels = ["RAM/panneau/etats/#",
                    "RAM/balanceMel/etats/#",
                    "RAM/alarmes/etats/#",
                    "RAM/powermeter/etats/#",
                    "RAM/shopvac/etats/#"];
var machineryState = [];

function updateMachinery(topic, value) {
  topic = topic.substr(topic.lastIndexOf('/') + 1, topic.length);
  for(let i = 0; i < machineryState.length; i++) {
    if(machineryState[i].topic == topic) {
      machineryState[i].value = value;
      return;
    }
  }
  machineryState.push({topic: topic, value : value});
}

function handleLogging(params, type, callback) {
  async.waterfall([
    function(callback) {

      let queryOptions;
      switch(type) {
        case "newAlarm":
            queryOptions = {
                from :  new Date() - new Date (serverInfo.creationDate),
                until : new Date(),
                limit : 1,
                start : 0,
                order : 'desc',
                fields : ['log_id']
            };
        break;
        case "alarmSeen":
            queryOptions = {
                limit : 1024,
                start : 0,
                order : 'desc',
                fields : ['level', 'message', 'log_id', 'timestamp', 'seenUser_id', 'seenUser_username', 'timestamp_seen']
            };
        break;
        case "newCommand":
            queryOptions = {
                from :  new Date() - new Date (serverInfo.creationDate),
                until : new Date(),
                limit : 1,
                start : 0,
                order : 'desc',
                fields : ['log_id']
            };
        break;
        case "getAlarms":
            queryOptions = {
                from : (typeof (params.from_time) === 'undefined') ? new Date() - new Date (serverInfo.creationDate) : new Date() - parseInt((params.from_time == "") ? new Date() - new Date (serverInfo.creationDate) : params.from_time),
                until : (typeof (params.until_time) === 'undefined') ? new Date() : new Date(params.until_time),
                limit : 1024,
                start : 0,
                order : 'desc',
                fields : ['level', 'message', 'log_id', 'timestamp', 'seenUser_id', 'seenUser_username', 'timestamp_seen']
            };
        break;
        case "getCommands":
            queryOptions = {
                from : (typeof (params.from_time) === 'undefined') ? new Date() - new Date (serverInfo.creationDate) : new Date() - parseInt((params.from_time == "") ? new Date() - new Date (serverInfo.creationDate) : params.from_time),
                until :  (typeof (params.until_time) === 'undefined') ? new Date() : new Date(params.until_time),
                limit : 1024,
                start : 0,
                order : 'desc',
                fields : ['level', 'message', 'log_id', 'timestamp','senderUser_id', 'senderUser_username']
            };
        break;
        case "getAllLogs":
            queryOptions = {
                from : (typeof (params.from_time) === 'undefined') ? new Date() - new Date (serverInfo.creationDate) : new Date() - parseInt((params.from_time == "") ? new Date() - new Date (serverInfo.creationDate) : params.from_time),
                until : (typeof (params.until_time) === 'undefined') ? new Date() : new Date(params.until_time),
                limit : 2048,
                start : 0,
                order : 'desc',
                fields : ['level', 'message', 'log_id', 'timestamp', 'seenUser_id']
            };
        break;
      }

      callback(null, queryOptions);

    },
    function(queryOptions, callback) {
      extfs.isEmpty(cmdlog_filename, function (empty) {
        if(empty) {
          extfs.isEmpty(alarmlog_filename, function (bothEmpty){
            if(bothEmpty) {
              if(type === "newAlarm" || type === "newCommand") {
                params.log_id = 0;
                logger.log((type === "newAlarm") ? 'warn' : 'info', params);

                if(type === "newAlarm") {
                  activeAlarms.push({log_id : params.log_id, message : params.message});
                }
              }
              callback(null, queryOptions, false);
            } else {
              callback(null, queryOptions, true);
            }
          });
        } else {
          callback(null, queryOptions, true);
        }
      });
    },
    function(queryOptions, filesInit, callback) {

      if(filesInit == false) {
        if(type === "newAlarm" || type === "newCommand") {
          callback(null, false, true);
        } else {
          callback(null, false, null);
        }
      } else {
        logger.query(queryOptions, function(err, rows) {
          if(err) throw err;
          if(type === "newAlarm"  || type === "newCommand") {
              let tmpRows = rows.cmdlog_file.concat(rows.alarmlog_file);
              if(tmpRows.length == 1) {
                params.log_id = tmpRows[0].log_id + 1;
              } else if (tmpRows.length > 1) {
                params.log_id = (tmpRows[0].log_id > tmpRows[1].log_id) ?
                                  tmpRows[0].log_id + 1 :
                                  tmpRows[1].log_id + 1;
              }

              logger.log((type === "newAlarm") ? 'warn' : 'info', params);

              if(type === "newAlarm") {
                activeAlarms.push({log_id : params.log_id, message : params.message});

                fs.readFile(cmdlog_filename, 'utf8', function (err, data) {
                  if(err) throw err;
                  let result = data.split('\n');

                  for(let i = 0; i < result.length; i++) {
                    if(result[i].indexOf("warn") != -1) {
                      result.splice(i, 1);
                    }
                  }

                  let resultStr = result.join('\n');
                  fs.writeFile(cmdlog_filename, resultStr, 'utf8', function (err) {
                    if (err) throw err;

                  });
                });
              }

              callback(null, false, true);

          } else if (type === "alarmSeen") {
              console.log((JSON.stringify(rows).slice(0, JSON.stringify(rows).indexOf("\"cmdlog_file")) + JSON.stringify(rows).slice(JSON.stringify(rows).indexOf("\"alarmlog_file"))));
              let passingRows = JSON.parse(JSON.stringify(rows).slice(0, JSON.stringify(rows).indexOf("\"cmdlog_file")) + JSON.stringify(rows).slice(JSON.stringify(rows).indexOf("\"alarmlog_file"))).alarmlog_file;//rows.alarmlog_file;
              //delete rows.cmdlog_file;
              console.log(passingRows);
              //console.log(rows.cmdlog_file);
              for(let i = 0; i < passingRows.length; i++)  {
                  if(passingRows[i].log_id == params.log_id) {
                      passingRows[i].seenUser_id = params.seenUser_id;
                      passingRows[i].seenUser_username = params.seenUser_username;
                      passingRows[i].timestamp_seen = new Date();
                  }
              }

              let rowsStr = JSON.stringify(passingRows);
              rowsStr = rowsStr.split('},').join('}\n');
              rowsStr = rowsStr.substr(1);
              rowsStr = rowsStr.slice(0, -1);

              //Clean array
              for(let i = 0; i < activeAlarms.length; i++) {

                  if(activeAlarms[i].log_id ==  params.log_id) {
                    activeAlarms.splice(i, 1);
                  }
              }

              callback(null, true, rowsStr);

          } else if (type === "getAlarms" || type === "getCommands" || type === "getAllLogs") {

              let passingRows;
              if(type === "getAlarms") {passingRows = rows.alarmlog_file;}
              else if(type === "getCommands") {passingRows = rows.cmdlog_file;}
              else if(type === "getAllLogs") {passingRows = rows.cmdlog_file.concat(JSON.parse(JSON.stringify(rows).slice(0, JSON.stringify(rows).indexOf("\"cmdlog_file")) + JSON.stringify(rows).slice(JSON.stringify(rows).indexOf("\"alarmlog_file"))).alarmlog_file);}

              //Apply word search filter
              if(typeof params.filter != 'undefined' && params.filter) {
                  for(let i = 0; i < passingRows.length; i++) {
                     if((passingRows[i].message.indexOf(params.filter) == -1 &&
                        passingRows[i].log_id.toString().indexOf(params.filter) == -1 &&
                        (passingRows[i].seenUser_id == null ? "" : passingRows[i].seenUser_id).toString().indexOf(params.filter) == -1 &&
                        (passingRows[i].seenUser_username == null ? "" : passingRows[i].seenUser_username).indexOf(params.filter) == -1 &&
                        (passingRows[i].seenUser_username == null ? "" : passingRows[i].seenUser_username).indexOf(params.filter) == -1 &&
                        (passingRows[i].timestamp_seen == null ? "" : passingRows[i].timestamp_seen).indexOf(params.filter) == -1 &&
                        (passingRows[i].timestamp == null ? "" : passingRows[i].timestamp).indexOf(params.filter) == -1 &&
                        type === "getAlarms") ||
                        (passingRows[i].message.indexOf(params.filter) == -1 &&
                         passingRows[i].log_id.toString().indexOf(params.filter) == -1 &&
                         (passingRows[i].senderUser_id == null ? "" : passingRows[i].senderUser_id).toString().indexOf(params.filter) == -1 &&
                         (passingRows[i].senderUser_username == null ? "" : passingRows[i].senderUser_username).indexOf(params.filter) == -1 &&
                         type === "getCommands") ||
                        (passingRows[i].message.indexOf(params.filter) == -1 &&
                         passingRows[i].log_id.toString().indexOf(params.filter) == -1 &&
                         type === "getAllLogs"))
                     {
                          passingRows.splice(i, 1); //Delete if search term not found in type context
                          if(passingRows.length == 1){
                            passingRows = [];
                          }
                     }
                  }
              }

              //Apply ordering constraint
              if(!(typeof params.order != 'undefined' && params.order)) {
                  passingRows.sort(function(a, b){
                      return (parseInt(a.log_id) > parseInt(b.log_id));
                  });
              } else if (params.order == "log_id" || params.order == "seenUser_id" || params.order == "senderUser_id") {
                  passingRows.sort(function(a, b){
                      return (parseInt(a[params.order]) > parseInt(b[params.order]));
                  });
              } else if (params.order == "seenUser_username" || params.orders == "senderUser_username") {
                  passingRows.sort(function(a, b){
                      return (a[params.order].localeCompare(b[params.order]));
                  });
              } else if (params.order == "timestamp" || params.order == "timestamp_seen") {
                  passingRows.sort(function(a, b){
                      return (new Date(a[params.order]) > new Date(b[params.order]));
                  });
              }
              callback(null, false, passingRows);
          }
        });
      }
    },
    function(writing, passedParam) {
      if(writing == false) {
        callback (passedParam);
      } else if(writing == true) {
        //Overwrite json file if an update was performed
        fs.writeFile(alarmlog_filename, passedParam, function(err) {
            if (err) throw err;
            callback(true);
        });
      }
  }],
  function(result) {
    callback(result);
  });
}

function handlePoolDb(req, type, query_addon, callback) {
  async.waterfall([
    function(callback){
      pool.getConnection(function(err, connection){
        if(err) {
          callback(true);
        } else {
          callback(null, connection);
        }
      });
    },
    function(connection, callback) {
      let querystr;
      switch(type) {
        case "login":
          querystr = "SELECT * FROM " + users_data_tab + " WHERE " +
                     "user_username = '" + req.body.user_username + "'";
        break;
        case "register":
          querystr = "INSERT INTO " + users_data_tab +
                     " (user_username, user_password, user_access, user_fname, user_lname) VALUES " +
                     "('"+ req.body.user_username + "', '" +  bcrypt.hashSync(req.body.user_password, saltRounds) + "', "+ req.body.user_access + ", '"+ req.body.user_fname +"', '"+ req.body.user_lname + "')";
        break;
        case "modify":
          querystr = "UPDATE " + users_data_tab + " SET " +
                     "user_username = '" + req.body.user_username + "', " +
                     "user_access = " + req.body.user_access + ", " +
                     "user_fname = '" + req.body.user_fname  + "', " +
                     "user_lname = '" + req.body.user_lname + "' "  +
                     "WHERE user_id = " + req.body.user_id;
        break;
        case "checkIfExists":
          querystr = "SELECT * FROM " + users_data_tab +
                     " WHERE user_username = '" + req.body.user_username + "'";
        break;
        case "modifyPassword":
          querystr = "UPDATE " + users_data_tab + " SET " +
                     "user_password = '" + bcrypt.hashSync(req.body.user_password, saltRounds) + "' " +
                     "WHERE user_id = " + req.body.user_id;
        break;
        case "delete":
          querystr = "DELETE FROM " + users_data_tab +
                    " WHERE user_id = " + req.params.user_id;
        break;
        case "checkPassword":
          querystr = "SELECT * FROM " + users_data_tab +
                     " WHERE user_username  = '" + req.body.user_username + "'";
        break;
        case "getUserProfile":
          querystr = "SELECT * FROM " + users_data_tab + " WHERE " +
                     "user_id = " + req.params.user_id;
        break;
        case "getAllUsers":
          querystr = "SELECT * FROM " + users_data_tab;
        break;
        default:
          querystr = type;
        break;
      }
      if(query_addon === "") {
        querystr += ";";
      }
      else {
        querystr += " " + query_addon + ";";
      }
      callback(null, connection, querystr);
    },
    function(connection, querystr, callback) {
      connection.query(querystr, function(err, rows) {
        connection.release();
        if(!err) {
          if(type === "login" || type === "getUserProfile") {
               callback(rows.length === 0 ? false : rows[0]);
          } else if (type === "checkPassword") {
               callback((rows.length === 0 || !bcrypt.compareSync(req.body.user_password, rows[0].user_password)) ? false : true);
          } else if(type === "getAllUsers") {
            callback(rows.length === 0 ? false : rows);
          } else if(type === "checkIfExists") {
            callback(rows.length === 0 ? "OK" : false);
          } else {
            callback(false);
          }
        } else {
          throw err;
          callback(true);
        }
      });
    }],
    function(result) {
      if(typeof(result) === 'boolean' && result === true) {
        callback(null);
      } else {
        callback(result);
      }
    });
}

function handleDbError(res, statusCode) {
  res.redirect(statusCode, '/db_error');
  throw new Error("Database error: code " + statusCode.toString());
}

function handleSessionDeath(req, res, requiredLevel) {
  if(!req.session.key) {
    res.redirect(401, '/expired');
  } else {
    if(req.session.key.user_access < requiredLevel) {
      res.redirect('/expired');
    }
  }

}

io.sockets.on('connection', function (socket) {

  socket.on('message', function(message) {
    console.log('Le client a un message pour vous : ' + message);
  });

  socket.on('dismissAlarm', function(message) {
    let messageArray = JSON.parse(message);
    for(let i = 0; i < activeAlarms.length; i++) {
      if(messageArray[0].log_id == activeAlarms[i].log_id) {
        let params = {
          seenUser_id : messageArray[0].seenUser_id,
          seenUser_username : messageArray[0].seenUser_username,
          log_id : messageArray[0].log_id
        };

        handleLogging(params, "alarmSeen", function(result){
            if(result != true) throw new Error('Logging system failure.');
        });
        activeAlarms.splice(i, 1);
      }
    }
  });

  socket.on('mqtt', function(message) {
    let messageArray = JSON.parse(message);
    for(let i = 0; i < messageArray.length; i++) {
      if(messageArray[i].topic.includes('cmd')) {
        let params = {
          message : messageArray[i].topic + ": " + messageArray[i].payload,
          senderUser_id : messageArray[i].senderUser_id,
          log_id : 0,
          senderUser_username : messageArray[i].senderUser_username
        };

        handleLogging(params, "newCommand", function(result) {
          if(result != true) throw new Error('Logging system failure.');
        });
      }
      updateMachinery(messageArray[i].topic, messageArray[i].payload);
      mqttClient.publish(messageArray[i].topic, messageArray[i].payload);
    }
  });

  socket.on('disconnect', function () {

  });

});

mqttClient.on('connect', function() {

  //Subscribe to all specified channels
  for(let i = 0; i < mqttChannels.length; i++)
  {
    mqttClient.subscribe(mqttChannels[i]);
  }
});

mqttClient.on('message', function(topic, payload) {

 if(topic.includes("RAM/alarmes/etats") && !payload.includes('?')) {
   let params = {
     message : topic + ": " + payload,
     seenUser_id : "",
     log_id : 0,
     seenUser_username : "",
     timestamp_seen : ""
   };

   handleLogging(params, "newAlarm", function(result) {
     if(result != true) throw new Error('Logging system failure.');
     io.sockets.emit('newAlarm_notif', JSON.stringify([activeAlarms.last()]));
   });
 } else if(topic.includes("RAM/alarmes/etats") && payload.includes('?')) {
    console.log(activeAlarms);
    let acknowledgements = {
      ALR_GB_OVF : {topic: "RAM/alarmes/statut/ALR_GB_OVF", payload: "true"},
      ALR_PB_OVF : {topic: "RAM/alarmes/statut/ALR_PB_OVF", payload: "true"},
      ALR_GB_NIV_MAX : {topic: "RAM/alarmes/statut/ALR_GB_NIV_MAX", payload: "true"},
      ALR_GB_NIV_MIN : {topic: "RAM/alarmes/statut/ALR_GB_NIV_MIN", payload: "true"},
      ALR_PB_NIV_MAX : {topic: "RAM/alarmes/statut/ALR_PB_NIV_MAX", payload: "true"},
      ALR_PB_NIV_MIN : {topic: "RAM/alarmes/statut/ALR_PB_NIV_MIN", payload: "true"},
      ALR_PB_TMP_MAX : {topic: "RAM/alarmes/statut/ALR_PB_TMP_MAX", payload: "true"},
      ALR_PB_TMP_MIN : {topic: "RAM/alarmes/statut/ALR_PB_TMP_MIN", payload: "true"}
    };

    for(let i = 0; i < activeAlarms.length; i++) {
      switch(activeAlarms[i].message) {
        case "RAM/alarmes/etats: ALR_GB_OVF":
          acknowledgements.ALR_GB_OVF.payload = "false";
        break;
        case "ALR_PB_OVF":
          acknowledgements.ALR_PB_OVF.payload = "false";
        break;
        case "ALR_GB_NIV_MAX":
          acknowledgements.ALR_GB_NIV_MAX.payload = "false";
        break;
        case "ALR_GB_NIV_MIN":
          acknowledgements.ALR_GB_NIV_MIN.payload = "false";
        break;
        case "ALR_PB_NIV_MAX":
          acknowledgements.ALR_PB_NIV_MAX.payload = "false";
        break;
        case "ALR_PB_NIV_MIN":
          acknowledgements.ALR_PB_NIV_MIN.payload = "false";
        break;
        case "ALR_PB_TMP_MAX":
          acknowledgements.ALR_PB_TMP_MAX.payload = "false";
        break;
        case "ALR_PB_TMP_MIN":
          acknowledgements.ALR_PB_TMP_MIN.payload = "false";
        break;
        default:

        break;
      }
    }

    mqttClient.publish(acknowledgements.ALR_GB_OVF.topic, acknowledgements.ALR_GB_OVF.payload);
    mqttClient.publish(acknowledgements.ALR_PB_OVF.topic, acknowledgements.ALR_PB_OVF.payload);
    mqttClient.publish(acknowledgements.ALR_GB_NIV_MAX.topic, acknowledgements.ALR_GB_NIV_MAX.payload);
    mqttClient.publish(acknowledgements.ALR_GB_NIV_MIN.topic, acknowledgements.ALR_GB_NIV_MIN.payload);
    mqttClient.publish(acknowledgements.ALR_PB_NIV_MAX.topic, acknowledgements.ALR_PB_NIV_MAX.payload);
    mqttClient.publish(acknowledgements.ALR_PB_NIV_MIN.topic, acknowledgements.ALR_PB_NIV_MIN.payload);
    mqttClient.publish(acknowledgements.ALR_PB_TMP_MAX.topic, acknowledgements.ALR_PB_TMP_MAX.payload);
    mqttClient.publish(acknowledgements.ALR_PB_TMP_MIN.topic, acknowledgements.ALR_PB_TMP_MIN.payload);

 } else {
   updateMachinery(topic, payload.toString());
   io.sockets.emit(topic, payload.toString());
 }


});

mqttClient.on('error', function(err) {
  if(err) throw err;
});



app.get('/logtest', function(req, res){

res.render('ejs/pages/conTest.ejs');


});

app.get('/', function(req, res) {
  res.render('ejs/pages/index.ejs');
});

app.get('/login', function (req, res) {
  res.render('ejs/pages/login.ejs', { error : false });
});

app.post('/login', function(req, res) {

  if((!req.body.user_username) || (!req.body.user_password)) {
	  res.render('ejs/pages/login.ejs', {error: true, errorDesc: "Tous les champs doivent être remplis."});
  } else {
   handlePoolDb(req, "login", "", function(response) {
     if(response === null) {
       throw new Error("Database error.");
     } else {
      	if(!response) {
				     res.render('ejs/pages/login.ejs', { error : true, errorDesc : "Le nom d'utilisateur est invalide."});
			    } else {
         if(bcrypt.compareSync(req.body.user_password, response.user_password)) {
            req.session.key = response;//userKey = response; <-- That's the backup
  				      res.redirect(301, '/home');
         } else {
          res.render('ejs/pages/login.ejs', { error : true, errorDesc : "Le mot de passe est invalide."});
         }
			    }
		   }
	  });
  }
});

app.get('/logout',function(req, res){
    if(req.session.key) {
      req.session.destroy(function(){
        res.render('ejs/pages/logout.ejs');
    });
    } else {
        res.redirect(401, '/expired');
    }
});

app.get('/expired', function(req, res) {
  res.render('ejs/pages/expired.ejs');
});

app.get('/error_404', function(req, res) {
  res.render('ejs/pages/error_404.ejs');
});

app.get('/error_500', function(req, res) {
  res.render('ejs/pages/error_500.ejs');
});


app.get('/register', function(req, res) {
  res.render('ejs/pages/register.ejs', { error : false });
});

app.post('/register', function(req, res) {
  if(req.body.user_password_repeat != req.body.user_password) {
	  res.render('ejs/pages/register.ejs', {error : true, errorDesc : "Les mots de passe ne correspondent pas."});
  }	else if ((!req.body.user_username) || (!req.body.user_password) || (!req.body.user_lname) || (!req.body.user_fname)) {
	  res.render('ejs/pages/register.ejs', {error : true, errorDesc : "Tous les champs doivent être remplis."});
  } else {
	handlePoolDb(req, "checkIfExists", "", function(response){
		if(response === null) {
			handleDbError(res, 500);
		} else if(response == false) {
			res.render('ejs/pages/register.ejs', { error : true, errorDesc : "Ce nom d'utilisateur est déjà pris. Veuillez en choisir un autre."});
		} else {
			handlePoolDb(req, "register", "", function(response) {
				if(response === null) {
					throw new Error("Database error.");
				} else {
					res.render('ejs/pages/register_done.ejs');
				}
			});
		}
	});
  }
});

app.get('/register_done', function(req, res){
  res.render('ejs/pages/register_done.ejs');
});

app.get('/users_list', function(req, res) {
  handleSessionDeath(req, res, 4);

  handlePoolDb(req, "getAllUsers", "", function(response) {
    if(response === null) {
      handleDbError(res, 500);
    } else if(!response) {
      handleDbError(res, 503);
    } else {
       res.render('ejs/pages/users_list.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, users_list : response, order_selected : " id"});
    }
  });
});

app.post('/users_list', function(req, res) {
  handlePoolDb(req, "getAllUsers", req.body.user_order, function(response) {
    if(response === null) {
      handleDbError(res, 500);
    } else if(!response) {
      handleDbError(res, 503);
    } else {
       res.render('ejs/pages/users_list.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, users_list : response, order_selected : req.body.user_order});
    }
  });
});

app.post('/users_action', function(req, res) {

  var user_id_target = req.body.redirectCommand.substring(4);

  if(req.body.redirectCommand.substring(0, 4) == 'dele') {
    res.redirect(301, '/delete_user/' + user_id_target);
  } else if(req.body.redirectCommand.substring(0, 4) == 'edit') {
    res.redirect(301, '/edit_user/' + user_id_target);
  } else if(req.body.redirectCommand.substring(0, 4) == 'pass') {
    res.redirect(301, '/password_user/' + user_id_target);
  } else if(req.body.redirectCommand.substring(0, 3) == 'add') {
    res.redirect(301, '/create_user');
  }
});

app.get('/create_user', function(req, res) {
  handleSessionDeath(req, res, 4);

	res.render('ejs/pages/create_user.ejs', {error : false, currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState});
});

app.post('/create_user', function(req, res) {
  if ((!req.body.user_username) || (!req.body.user_password) || (!req.body.user_lname) || (!req.body.user_fname)) {
	  res.render('ejs/pages/create_user.ejs', {error : true, errorDesc : "Tous les champs doivent être remplis.", currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState});
  } else {
	handlePoolDb(req, "checkIfExists", "", function(response) {
		if(response === null) {
			handleDbError(res, 500);
		} else if(response == false) {
			res.render('ejs/pages/create_user.ejs', {error: true, errorDesc : "Ce nom d'utilisateur est déjà pris.", currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState});
		} else {
			handlePoolDb(req, "register", "", function(response) {
				if(response === null) {
					handleDbError(res, 500);
				} else {
					res.redirect(301, '/users_list');
				}
			});
		}
	});
  }
});

app.get('/edit_user/:user_id', function(req, res) {
  handleSessionDeath(req, res, 4);

  handlePoolDb(req, "getUserProfile", "", function(response) {
    if(response === null) {
			handleDbError(res, 500);
		} else if(response == false) {
      res.render('ejs/pages/edit_user_invalid.ejs', {currentUser : req.session.key});
    } else {
        res.render('ejs/pages/edit_user.ejs', {error : false, currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, editedUser : response });
    }
  });
});

app.post('/edit_user', function(req, res) {
  handleSessionDeath(req, res, 4);
  //req.params = {user_id : req.body.user_id };

  if((!req.body.user_username) || (!req.body.user_lname) || (!req.body.user_fname)) {
    res.render('ejs/pages/edit_user.ejs', {error : true, errorDesc : "Tous les champs doivent être remplis.", currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, editedUser : req.body});
  } else {
    handlePoolDb(req, "modify", "", function(response) {
      if(response === null) {
        handleDbError(res, 500);
      } else {
        res.redirect(301, "users_list");
      }
    });
  }



});

app.get('/password_user/:user_id', function(req, res) {
  handleSessionDeath(req, res, 4);

  handlePoolDb(req, "getUserProfile", "", function(response) {
    if(response === null) {
			handleDbError(res, 500);
	  } else if(response == false) {
      res.render('ejs/pages/edit_user_invalid.ejs', {currentUser : req.session.key});
    } else {
      res.render('ejs/pages/password_user.ejs', {error : false, currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, editedUser : response});
    }
  });
});

app.post('/password_user', function(req, res) {
  handlePoolDb(req, "modifyPassword", "", function(response) {
    if(response === null) {
      handleDbError(res, 500);
    } else {
      res.redirect(301, '/users_list');
    }
  });
});

app.get('/delete_user/:user_id', function(req, res) {
  handleSessionDeath(req, res, 4);

  handlePoolDb(req, "delete", "", function(response) {
    if(response === null) {
      handleDbError(res, 500);
    } else {
      res.redirect(301, '/users_list');
    }
  });
});

app.get('/alarms', function(req, res) {
  handleSessionDeath(req, res, 4);

  handleLogging({}, "getAlarms", function(result) {
    res.render('ejs/pages/alarms.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, alarms : result, savedDateMode: 0, presetParams : {order : "", filter : "", from_time : ""}});
  });
});

app.post('/alarms', function(req, res) {
  handleLogging(req.body, "getAlarms", function(result) {
    res.render('ejs/pages/alarms.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, alarms : result, savedDateMode: req.body.dateMode_save, presetParams : req.body});
  });

});

app.get('/commands', function(req, res) {
  handleSessionDeath(req, res, 4);

  handleLogging({}, "getCommands", function(result) {
    res.render('ejs/pages/commands.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, commands : result, savedDateMode: 0, presetParams : {order : "", filter : "", from_time : ""}});
  });
});

app.post('/commands', function(req, res) {
  handleLogging(req.body, "getCommands", function(result) {
    res.render('ejs/pages/commands.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, commands : result, savedDateMode: req.body.dateMode_save, presetParams : req.body});
  });
});

app.get('/logs', function(req, res) {
  handleSessionDeath(req, res, 4);

  handleLogging({}, "getAllLogs", function(result) {
    res.render('ejs/pages/logs.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, logs : result, savedDateMode: 0, presetParams : {order : "", filter : "", from_time : ""}});
  });
});

app.post('/logs', function(req, res){
  handleLogging(req.body, "getAllLogs", function(result) {
    res.render('ejs/pages/logs.ejs',  {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState, logs : result, savedDateMode: req.body.dateMode_save, presetParams : req.body});
  });
});

app.get('/panel', function(req, res){
  handleSessionDeath(req, res, 0);
  console.log(machineryState);
  res.render('ejs/pages/panel.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState});

});

app.get('/mixer', function(req, res) {
  handleSessionDeath(req, res, 0);

  res.render('ejs/pages/mixerPLC.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState});
});

app.get('/alarms-set', function(req, res) {
  handleSessionDeath(req, res, 3);

  res.render('ejs/pages/alarms_setting.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState});
});

app.get('/cameras', function(req, res,) {
  handleSessionDeath(req, res, 2);

  res.render('ejs/pages/cameras.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState});
});

app.get('/home', function(req, res) {
  handleSessionDeath(req, res, 0);

  res.render('ejs/pages/home.ejs', {currentUser : req.session.key, serverInfo : serverInfo, activeAlarms : activeAlarms, machineryState : machineryState});
});

app.get('/db_error', function(req, res) {
    res.render('ejs/pages/error_500.ejs');
});

app.use(function(req,res) {
  res.render('ejs/pages/error_404.ejs');
});

ON_DEATH(function(signal, err) {
  if (err) throw err;

  mqttClient.end();
  process.exit(0);
});
