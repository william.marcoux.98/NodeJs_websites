const express = require('express');
const session = require('express-session');
const redirect = require('express-redirect');
const bodyParser = require('body-parser');
const crypto = require('crypto');
const app = express();
const server = app.listen(8080);
const io = require('socket.io').listen(server);
const ON_DEATH = require('death');
const mysql = require('mysql');

const SIDVerifInterval = 10 * 60 * 1000; //10 minutes
const SIDVerif = setInterval(SIDVerifTimer, SIDVerifInterval);

var disconnectTimeOut;

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "panneauRAM"
});

con.connect(function(err) {
  if (err) throw err;

});

function cleanUpSIDs() {
  console.log('Disconnecting');

  let querystr = "SELECT (SID) FROM SIDs WHERE ExpTime <= NOW();"

  con.query(querystr, function (err, result) {
      if (err) throw err;

      querystr = "UPDATE users SET SID = NULL WHERE ";
      for(let i=0;i<result.length;i++)
      {
        querystr += "SID = '" + result[i].SID + "'";

        if( i == result.length - 1) {
          querystr += ";";
        } else {
          querystr +=" || ";
        }
      }

      //SIDs in users table are reset
      con.query(querystr, function(err, result){
        if(err) throw err;

        querystr = "DELETE FROM SIDs WHERE ExpTime <= NOW();"
        //Expired SIDs are deleted
        con.query(querystr, function(err, result){
          if(err)throw err;

        });

      });
  });
}

io.sockets.on('connection', function (socket) {
  clearTimeout(disconnectTimeOut);

  socket.on('disconnect', function () {
   //Function executes after 5 seconds if no reconnection is detected
   disconnectTimeOut = setTimeout(cleanUpSIDs, 5 * 1000);
 });

});

function SIDVerifTimer() {
  //Do an SID database checkup routinely
  cleanUpSIDs();
}

redirect(app);
app.set('view engine', 'ejs');

app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge : 60 * 1000,
  }
}));

app.use(function (req, res, next) {

  if(!req.session.cookie.sessionID) {
    //Do SID cleanup and logout
    cleanUpSIDs();
  }

  next();
});

app.get('/foo', function (req, res, next) {
  res.send('time remaining: ' + req.session.cookie.maxAge + 'and you are ' + req.session.logged);
});

app.get('/login', function (req, res, next) {
  req.session.cookie.sessionID = crypto.randomBytes(20).toString('hex');
  
  res.send('time remaining: ' + req.session.cookie.maxAge);
});

app.get('/logout', function (req, res, next){
  //Logout and SID deletion
  let querystr = "";
  if(req.session.cookie.sessionID){
    querystr = "DELETE FROM SIDs WHERE SID ='" + req.session.cookie.sessionID + "';";

    con.query(querystr, function(err, result){
      if(err)throw err;

      querystr = "UPDATE users SET SID = NULL WHERE SID = '" + req.session.cookie.sessionID "';";
      con.query(querystr, function(err, result){
        if(err)throw err;

        req.session.destroy();
        res.redirect(301, '/logout');
      });
    });

  }

});

app.get('/conTest', function(req, res, next){
  res.render('pages/conTest.ejs');
});
app.get('/conTest0', function(req, res, next){
  res.render('pages/conTest0.ejs');
});

ON_DEATH(function(signal, err) {
  //Delete all SIDs
  let querystr = "UPDATE users SET SID = NULL";
  //Reset SIDs in users
  con.query(querystr, function(err, result){
    if(err)throw err;

    querystr = "DELETE FROM SIDs";
    //Reset SID table
    con.query(querystr, function(err, result){
      if(err) throw err;

      process.exit(0);
    });
  });

});