const async = require('async');
const express = require('express');
const session = require('express-session');
const redirect = require('express-redirect');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const app = express();
const server = app.listen(8080);
const io = require('socket.io').listen(server);
const ON_DEATH = require('death');
const mysql = require('mysql');
const redis = require('redis');
const redisStore = require('connect-redis')(session);

var client = redis.createClient();
const users_data_db = 'panel_users';
const logs_data_db = 'panel_logs';
const users_data_tab = 'users_data';
const users_status_tab = 'users_status'; //useless for now
const log_alarms_tab = 'alarm_log';
const log_cmd_tab = 'cmd_log';
const saltRounds = 10;

//Backup solution in case of session bug
var userKey = false;

const pool = mysql.createPool({
      connectionLimit: 50,
      host: 'localhost',
      user: 'root',
      password: 'root',
      database: users_data_db,
      debug: false
});

const log = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: logs_data_db,
  multipleStatements: true
});

app.set('view engine', 'ejs');
app.use("/public",express.static(__dirname + "/public"));
app.use("/node_modules",express.static(__dirname + "/node_modules"));
app.set('views', __dirname + '/views');

app.use( session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false,
  store: new redisStore({
    host: 'localhost',
    port: 6379,
    client: client,
    ttl: 60 * 1000
  })
}));

redirect(app);
app.use(cookieParser("secret"));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

function handleLogDb(params, type, query_addon, callback) {
  async.waterfall([
    function(callback) {
      let querystr;
      switch(type) {
        case "newAlarm":
          querystr = "INSERT INTO " + log_alarms_tab +
                     "()"
        break;
        case "alarmSeen":

        break;
        case "delAllAlarms":

        break;
        case "delAlarmsCnt":

        break;
        case "new_cmd":

        break;
        case "getAllAlarms":

        break;
        case "getAllCmds":

        break;
        case "getTimeLine":

        break;
      }
      if(query_addon === "") {
        querystr += ";";
      }
      else {
        querystr += " " + query_addon + ";";
      }
      callback(null, querystr);
    },
    function(querystr, callback) {
      connection.query(querystr, function(err, rows) {
        if(err) throw err;


      });
    }],
  function(result) {
    callback(result);
  });
}

function handlePoolDb(req, type, query_addon, callback) {
  async.waterfall([
    function(callback){
      pool.getConnection(function(err, connection){
        if(err) {
          callback(true);
        } else {
          callback(null, connection);
        }
      });
    },
    function(connection, callback) {
      let querystr;
      switch(type) {
        case "login":
          querystr = "SELECT * FROM " + users_data_tab + " WHERE " +
                     "user_username = '" + req.body.user_username + "'";
        break;
        case "register":
          querystr = "INSERT INTO " + users_data_tab +
                     " (user_username, user_password, user_access, user_fname, user_lname) VALUES " +
                     "('"+ req.body.user_username + "', '" +  bcrypt.hashSync(req.body.user_password, saltRounds) + "', "+ req.body.user_access + ", '"+ req.body.user_fname +"', '"+ req.body.user_lname + "')";
        break;
        case "modify":
          querystr = "UPDATE " + users_data_tab + " SET " +
                     "user_username = '" + req.body.user_username + "', " +
                     "user_access = " + req.body.user_access + ", " +
                     "user_fname = '" + req.body.user_fname  + "', " +
                     "user_lname = '" + req.body.lname + "' "  +
                     "WHERE user_id = " + req.body.user_id;
        break;
        case "checkIfExists":
          querystr = "SELECT * FROM " + users_data_tab +
                     " WHERE user_username = '" + req.body.user_username + "'";
        break;
        case "modifyPassword":
          querystr = "UPDATE " + users_data_tab + " SET " +
                     "user_password = '" + bcrypt.hashSync(req.body.user_password, saltRounds) + "' " +
                     "WHERE user_id = " + req.body.user_id;
        break;
        case "delete":
          querystr = "DELETE FROM " + users_data_tab +
                    " WHERE user_id = " + req.params.user_id;
        break;
        case "checkPassword":
          querystr = "SELECT * FROM " + users_data_tab +
                     " WHERE user_username  = '" + req.body.user_username + "'";
        break;
        break;
        case "getUserProfile":
          querystr = "SELECT * FROM " + users_data_tab + " WHERE " +
                     "user_id = " + req.params.user_id;
        break;
        case "getAllUsers":
          querystr = "SELECT * FROM " + users_data_tab;
        break;
        case "addStatus":
          querystr = "INSERT INTO " + users_status_tab +
                     "(user_id, user_status) VALUES " +
                     "(" + req.session.key["user_id"] + ", '" + req.body.status + "')";
        break;
        case "getStatus":
          querystr = "SELECT * FROM " + user_status_tab + " WHERE " +
                     "user_id = " + req.session.key["user_id"];
        break;
        default:
          querystr = type;
        break;
      }
      if(query_addon === "") {
        querystr += ";";
      }
      else {
        querystr += " " + query_addon + ";";
      }
      callback(null, connection, querystr);
    },
    function(connection, querystr, callback) {
      connection.query(querystr, function(err, rows) {
        connection.release();
        if(!err) {
          if(type === "login" || type === "getUserProfile") {
               callback(rows.length === 0 ? false : rows[0]);
          } else if (type === "checkPassword") {
               callback((rows.length === 0 || !bcrypt.compareSync(req.body.user_password, rows[0].user_password)) ? false : true);
          } else if(type === "getStatus" || type === "getAllUsers") {
            callback(rows.length === 0 ? false : rows);
          } else if(type === "checkIfExists") {
            callback(rows.length === 0 ? "OK" : false);
          } else {
            callback(false);
          }
        } else {
          callback(true);
        }
      });
    }],
    function(result) {
      if(typeof(result) === 'boolean' && result === true) {
        callback(null);
      } else {
        callback(result);
      }
    });
}

function handleDbError(res, statusCode) {
  throw new Error("Database error: code " + statusCode.toString());
  res.redirect(statusCode, '/db_error');
}

function handleSessionDeath(req, res) {
  if(!req.session.key) {
    res.redirect(401, '/expired');
  }
}

io.sockets.on('connection', function (socket) {

  socket.on('message', function(message) {
    console.log('Le client a un message pour vous : ' + message);
  });


  socket.on('disconnect', function () {

 });

});

app.get('/login', function (req, res, next) {
  res.render('ejs/pages/login.ejs', { error : false });
});

app.post('/login', function(req, res, next) {

  if((!req.body.user_username) || (!req.body.user_password)) {
	  res.render('ejs/pages/login.ejs', {error: true, errorDesc: "Tous les champs doivent être remplis."});
  } else {
	handlePoolDb(req, "login", "", function(response) {
		if(response === null) {
			throw new Error("Database error.");
		} else {
			if(!response) {
				res.render('ejs/pages/login.ejs', { error : true, errorDesc : "Le nom d'utilisateur est invalide."});
			} else {
        if(bcrypt.compareSync(req.body.user_password, response.user_password)) {
          req.session.key = response;//userKey = response; <-- That's the backup
  				res.redirect(301, '/home');
        } else {
          res.render('ejs/pages/login.ejs', { error : true, errorDesc : "Le mot de passe est invalide."});
        }
			}
		}
	});
  }

});

app.get('/logout',function(req, res){
    if(req.session.key) {
      req.session.destroy(function(){
        res.render('ejs/pages/logout.ejs');
    });
    } else {
        res.redirect(401, '/expired');
    }
});

app.get('/expired', function(req, res) {
  res.render('ejs/pages/expired.ejs');
});

app.get('/register', function(req, res) {
  res.render('ejs/pages/register.ejs', { error : false });
});

app.post('/register', function(req, res) {
  if(req.body.user_password_repeat != req.body.user_password) {
	  res.render('ejs/pages/register.ejs', {error : true, errorDesc : "Les mots de passe ne correspondent pas."});
  }	else if ((!req.body.user_username) || (!req.body.user_password) || (!req.body.user_lname) || (!req.body.user_fname)) {
	  res.render('ejs/pages/register.ejs', {error : true, errorDesc : "Tous les champs doivent être remplis."});
  } else {
	handlePoolDb(req, "checkIfExists", "", function(response){
		if(response === null) {
			handleDbError(res, 500);
		} else if(response == false) {
			res.render('ejs/pages/register.ejs', { error : true, errorDesc : "Ce nom d'utilisateur est déjà pris. Veuillez en choisir un autre."});
		} else {
			handlePoolDb(req, "register", "", function(response) {
				if(response === null) {
					throw new Error("Database error.");
				} else {
					res.render('ejs/pages/register_done');
				}
			});
		}
	});
  }
});

app.get('/users_list', function(req, res) {
  handleSessionDeath(req, res);

  handlePoolDb(req, "getAllUsers", "", function(response) {
    if(response === null) {
      handleDbError(res, 500);
    } else if(!response) {
      handleDbError(res, 503);
    } else {
       res.render('ejs/pages/users_list.ejs', {currentUser : req.session.key, users_list : response, order_selected : " id"});
    }
  });
});

app.post('/users_list', function(req, res) {
  handlePoolDb(req, "getAllUsers", req.body.user_order, function(response) {
    if(response === null) {
      handleDbError(res, 500);
    } else if(!response) {
      handleDbError(res, 503);
    } else {
       res.render('ejs/pages/users_list.ejs', {currentUser : req.session.key, users_list : response, order_selected : req.body.user_order});
    }
  });
});

app.post('/users_action', function(req, res) {

  var user_id_target = req.body.redirectCommand.substring(4);

  if(req.body.redirectCommand.substring(0, 4) == 'dele') {
    console.log("hello");
    res.redirect(301, '/delete_user/' + user_id_target);
  } else if(req.body.redirectCommand.substring(0, 4) == 'edit') {
    res.redirect(301, '/edit_user/' + user_id_target);
  } else if(req.body.redirectCommand.substring(0, 4) == 'pass') {
    res.redirect(301, '/password_user/' + user_id_target);
  } else if(req.body.redirectCommand.substring(0, 3) == 'add') {
    res.redirect(301, '/create_user');
  }
});

app.get('/create_user', function(req, res) {
  handleSessionDeath(req, res);

	res.render('ejs/pages/create_user.ejs', {error : false});
});

app.post('/create_user', function(req, res) {
  if ((!req.body.user_username) || (!req.body.user_password) || (!req.body.user_lname) || (!req.body.user_fname)) {
	  res.render('ejs/pages/create_user.ejs', {error : true, errorDesc : "Tous les champs doivent être remplis."});
  } else {
	handlePoolDb(req, "checkIfExists", "", function(response) {
		if(response === null) {
			handleDbError(res, 500);
		} else if(response == false) {
			res.render('ejs/pages/create_user.ejs', {error: true, errorDesc : "Ce nom d'utilisateur est déjà pris."})
		} else {
			handlePoolDb(req, "register", "", function(response) {
				if(response === null) {
					handleDbError(res, 500);
				} else {
					res.redirect(301, '/users_list');
					console.log("New user created!");
				}
			});
		}
	});
  }
});

app.get('/edit_user/:user_id', function(req, res) {
  handleSessionDeath(req, res);

  handlePoolDb(req, "getUserProfile", "", function(response) {
    if(response === null) {
			handleDbError(res, 500);
		} else if(response == false) {
      res.render('ejs/pages/edit_user_invalid.ejs', {currentUser : req.session.key});
    } else {
        res.render('ejs/pages/edit_user.ejs', {error : false, currentUser : req.session.key, editedUser : response });
    }
  });
});

app.get('/password_user/:user_id', function(req, res) {
  handleSessionDeath(req, res);

  handlePoolDb(req, "getUserProfile", "", function(response) {
    if(response === null) {
			handleDbError(res, 500);
	  } else if(response == false) {
      res.render('ejs/pages/edit_user_invalid.ejs', {currentUser : req.session.key});
    } else {
      res.render('ejs/pages/password_user.ejs', {error : false, currentUser : req.session.key, editedUser : response});
    }
  });
});

app.post('/password_user', function(req, res) {
  handlePoolDb(req, "modifyPassword", "", function(response) {
    if(response === null) {
      handleDbError(res, 500);
    } else {
      res.redirect(301, '/users_list');
    }
  });
});

app.get('/delete_user/:user_id', function(req, res) {
  handleSessionDeath(req, res);

  handlePoolDb(req, "delete", "", function(response) {
    if(response === null) {
      handleDbError(res, 500);
    } else {
      res.redirect(301, '/users_list');
    }
  });
});

app.get('/home', function(req, res) {
  handleSessionDeath(req, res);

  res.render('ejs/pages/home.ejs', {currentUser : req.session.key});
});

app.get('/db_error', function(req, res) {
    res.render('ejs/pages/error_db.ejs');
});

ON_DEATH(function(signal, err) {
  //Delete all SIDs

process.exit(0);
});
