const async = require('async');
const express = require('express');
const session = require('express-session');
const redirect = require('express-redirect');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const app = express();
const server = app.listen(8080);
const io = require('socket.io').listen(server);
const ON_DEATH = require('death');
const mysql = require('mysql');
const redis = require('redis');
const redisStore = require('connect-redis')(session);
var client = redis.createClient();
const users_data_db = 'panel_users';
const users_data_tab = 'users_data';
const users_status_tab = 'users_status'; //useless for now
const saltRounds = 10;

const pool  = mysql.createPool({
      connectionLimit: 50,
      host: 'localhost',
      user: 'root',
      password: 'root',
      database: users_data_db,
      debug: false
});

app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  store: new redisStore({
    host: 'localhost',
    port: 6379,
    client: client,
    ttl: 60 * 1000
  })
}));

redirect(app);
app.set('view engine', 'ejs');
app.use("/public",express.static(__dirname + "/public"));
app.use("/node_modules",express.static(__dirname + "/node_modules"));
app.set('views', __dirname + '/views');
app.use(cookieParser("secret"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

function handlePoolDb(req, type, callback) {
  async.waterfall([
    function(callback){
      pool.getConnection(function(err, connection){
        if(err) {
          callback(true);
        } else {
          callback(null, connection);
        }
      });
    },
    function(connection, callback) {
      var querystr;
      switch(type) {
        case "login":
          querystr = "SELECT * FROM +" + users_data_db + " WHERE " +
                     "user_username = '" + req.body.user_username + "';";
        break;
        case "register": //Remember to replace req.body.password with encrypted
          querystr = "INSERT INTO " + users_data_tab +
                     "(user_username, user_password, user_access, user_fname, user_lname) VALUES " +
                     "('"+ req.body.user_username + "', '" +  bcrypt.hashSync(req.body.user_password, saltRounds) + "', "+ req.body.user_access.toString() + ", '"+ req.body.user_fname +"', '"+ req.body.user_lname + "');";
        break;
        case "modify":
          querystr = "UPDATE " + users_data_tab + " SET " +
                     "user_username = '" + req.body.user_username + "', " +
                     "user_access = " + req.body.user_access + ", " +
                     "user_fname = '" + req.body.user_fname  + "', " +
                     "user_lname = '" + req.body.lname + "' "  +
                     "WHERE user_id = " + req.body.user_id + ";";
        break;
        case "checkIfExists":
          querystr = "SELECT * FROM " + users_data_tab +
                     " WHERE user_username = '" + req.body.user_username + "';";
        break;
        case "modifyPassword":
          querystr = "UPDATE " + users_data_tab + " SET "+
                     "user_password = '" + bcrypt.hashSync(req.body.user_password, saltRounds) + "' " +
                     "WHERE user_id = " + req.body.user_id + ";";
        break;
        case "delete":
          querystr = "DELETE FROM " + users_data_tab +
                    " WHERE user_id = " + req.body.user_id + ";";
        break;
        case "checkPassword":
          querystr = "SELECT * FROM " + users_data_tab +
                     " WHERE user_username  = '" + req.body.user_username + "';";
        break;
        break;
        case "getUserProfile":
          querystr = "SELECT * FROM +" + users_data_tab + " WHERE " +
                     "user_id = '" + req.body.user_id + "';";
        break;
        case "getAllUsers":
          querystr = "SELECT * FROM " + users_data_tab + ";";
        break;
        case "addStatus":
          querystr = "INSERT INTO " + users_status_tab +
                     "(user_id, user_status) VALUES " +
                     "(" + req.session.key["user_id"] + ", '" + req.body.status + "');";
        break;
        case "getStatus":
          querystr = "SELECT * FROM " + user_status_tab + " WHERE " +
                     "user_id = " + req.session.key["user_id"] + ";";
        break;
        default:
        break;
      }
      callback(null, connection, querystr);
    },
    function(connection, querystr, callback) {
      connection.query(querystr, function(err, rows) {
        connection.release();
        if(!err) {
          if(type === "login" || type === "getUserProfile") {
               callback(rows.length === 0 ? false : rows[0]);
          } else if (type === "checkPassword") {
               callback((rows.length === 0 || !bcrypt.compareSync(req.body.user_password, rows[0].user_password)) ? false : true);
          } else if(type === "getStatus" || type === "getAllUsers") {
            callback(rows.length === 0 ? false : rows);
          } else if(type === "checkIfExists") {
            callback(rows.length === 0 ? false : true);
          } else {
            callback(false);
          }
        } else {
          callback(true);
        }
      });
    }],
    function(result) {
      if(typeof(result) === 'boolean' && result === true) {
        callback(null);
      } else {
        callback(result);
      }
    });
}

io.sockets.on('connection', function (socket) {
//  clearTimeout(disconnectTimeOut);

  socket.on('message', function(message) {
    console.log('Le client a un message pour vous : ' + message);
  });


  socket.on('disconnect', function () {

 });

});

app.get('/login', function (req, res, next) {
  res.render('pages/login.ejs', { errorCode : 0 });
});

app.post('/login', function(req, res, next) {
  handlePoolDb(req, "login", function(response){
    if(response === null) {
      console.log("Database error.");
    } else {
      if(!response) {
        console.log("Login failed, invalid infos.");
        res.render('ejs/pages/login.ejs', { errorCode : 1 });
      } else {
        req.session.key = response;
        res.redirect(301, '/home');
        console.log("Login successful");
      }
    }
  });
});

app.get('/logout',function(req,res){
    if(req.session.key) {
      req.session.destroy(function(){
        res.render('ejs/pages/logout.ejs');
    });
    } else {
        res.redirect(301, '/expired');
    }
});

app.get('/expired', function(req, res) {
  res.render('ejs/pages/expired.ejs');
});

app.get('/register', function(req, res){
  res.render('ejs/pages/register.ejs', {errorCode : 0 });
});

app.post('/register', function(req, res){
  handlePoolDb(req, "checkIfExists", function(response){
    if(response === null) {
      console.log("Database error.");
    } else if(response == false) {
      console.log("User already exists.");
    } else {
      handlePoolDb(req, "register", function(response){
        if(response === null) {
          console.log("Database error.");
        } else {
          console.log("New user created!");
        }
      });
    }
  });
});

app.get('/users_list', function(req, res) {

  if(!req.session.key) {
    res.redirect(301, '/expired');
  }

  handlePoolDb(req, "getAllUsers", function(req, res) {
    if(response === null) {
      console.log("Database error.");
    } else if(!response) {
      console.log("No existing users");
    } else {
      console.log("Here's the users");
    }
  });
});

app.post('/create_user', function(req, res){
  handlePoolDb(req, "checkIfExists", function(response) {
    if(response === null) {
      console.log("Database error.");
    } else if(response == false) {
      console.log("User already exists.");
    } else {
      handlePoolDb(req, "register", function(response){
        if(response === null) {
          console.log("Database error.");
        } else {
          console.log("New user created!");
        }
      });
    }
  });
});

app.get('/db_error', function(req, res) {
    res.render('ejs/pages/error_db.ejs');
});

ON_DEATH(function(signal, err) {
  //Delete all SIDs

process.exit(0);
});
