function loadInfos(machinery) {
    for(let i = 0; i < machinery.length; i++) {
        if(machinery[i].topic == "NivLhGB" ||
           machinery[i].topic == "NivLbGB" ||
           machinery[i].topic == "NivLhPB" ||
           machinery[i].topic == "NivLbPB" ||
           machinery[i].topic == "TempLhPB" ||
           machinery[i].topic == "TempLbPB") {
            document.getElementById("num_" + machinery[i].topic).value = machinery[i].value;
        }
    }
}

function submitAlarm(topic) {
    let value = document.getElementById("num_" + topic).value;
    if(value == "") {return;}
    socket.emit('mqtt', JSON.stringify([{topic : "RAM/alarmes/cmd/" + topic, payload : value.toString()}]));
}

$(document).ready(function() {
    loadInfos(JSON.parse(document.getElementById('machineryStateParser').innerHTML));
});
