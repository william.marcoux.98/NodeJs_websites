var  redirCommand = function(strCommand) {
            document.getElementById("redirectCommand").value = strCommand;
            document.getElementById("users_action_form").submit();
};

var serverInfo = JSON.parse(document.getElementById('serverInfoParser').innerHTML);
var userInfo = JSON.parse(document.getElementById('userInfoParser').innerHTML);

var socket = io.connect(serverInfo.IPv4 + ":8080");

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
