var alarmsQty = JSON.parse(document.getElementById('activeAlarmsArray').innerHTML).length;

$(document).ready(function(){
    if(alarmsQty > 0) {
      $('#myModal').modal('show');
    }

});


function dismissAlarm(div_id, log_id, seenUser_id, seenUser_username) {
    $('#' + div_id).hide();

    socket.emit('dismissAlarm', JSON.stringify([{log_id : log_id, seenUser_id : seenUser_id, seenUser_username : seenUser_username}]));

    alarmsQty--;
    if(alarmsQty == 0) {
       $('#myModal').modal('hide');
    }
}

function hideAlarm(div_id) {
    $('#' + div_id).hide();

    alarmsQty--;
    if(alarmsQty == 0) {
       $('#myModal').modal('hide');
    }
}

socket.on('newAlarm_notif', function(message) {
  newAlarms = JSON.parse(message);
  oldAlarms = JSON.parse(document.getElementById('activeAlarmsArray').innerHTML);

  for(let i = 0; i < newAlarms.length; i++) {
    if (oldAlarms.filter(function(e) { return e.log_id == newAlarms.log_id; }).length == 0) {
      //oldAalrms.length + i
      alert(newAlarms[i].log_id);
      $("#alarm_popup_body").append("<div id='alarmDiv" + (oldAlarms.length + i).toString() + "'>" +
                                      "<p> ID: " + newAlarms[i].log_id + "</p>" +
                                      "<p> Message: " + newAlarms[i].message + "</p>" +
                                      "<button type='button' class='btn btn-default' onclick=\"hideAlarm('alarmDiv" + (oldAlarms.length + i).toString() + "');\"> Je me délègue </button> " +
                                      "<button type='button' class='btn btn-primary' onclick=\"dismissAlarm('alarmDiv" + (oldAlarms.length + i).toString() +  "'," + newAlarms[i].log_id.toString() + " , " + userInfo.user_id.toString() + ", '" + userInfo.user_username +"');\"> Je m'en charge </button>" +
                                    "</div>");
      alarmsQty++;
    }
  }

  $("#myModal").modal('show');

});

/*
<div id=<%= "alarmDiv" + i.toString() %>>
    <p> ID: <%= activeAlarms[i].log_id %></p>
    <p> Message: <%= activeAlarms[i].message %></p>
    <button type="button" class="btn btn-default" onclick=<%=  "hideAlarm('alarmDiv" + i.toString() + "');" %>> Je me délègue </button>
    <button type="button" class="btn btn-primary" onclick=<%= "dismissAlarm('alarmDiv" + i.toString() + "'," + activeAlarms[i].log_id.toString() + ","+ currentUser.user_id.toString() + ",'" + currentUser.user_username + "');"%>> Je m'en charge </button>
</div>*/
