
function setDisplayAttributes(display) {
 display.pattern        = "#####.#";
 display.displayAngle    = 6;
 display.digitHeight     = 20;
 display.digitWidth      = 14;
 display.digitDistance   = 2.5;
 display.segmentWidth    = 2;
 display.segmentDistance = 0.3;
 display.segmentCount    = 7;
 display.cornerType      = 3;
 display.colorOn         = "#ffffff";
 display.colorOff        = "#0c0c0c";
 display.setValue('000.0000');
 display.draw();
}

var display_Poids = new SegmentDisplay('display_Poids');
setDisplayAttributes(display_Poids);
var display_Tare = new SegmentDisplay('display_Tare');
setDisplayAttributes(display_Tare);

var currentMode;
const RAMTopic = "RAM/balanceMel/cmd/";

function loadInfos(machinery) {
  for(let i = 0; i < machinery.length; i++) {
    switch(machinery[i].topic) {
      case "RecetteA":
        document.getElementById('num_RecetteA').value = machinery[i].value;
      break;
      case "RecetteB":
        document.getElementById('num_RecetteB').value = machinery[i].value;
      break;
      case "RecetteC":
        document.getElementById('num_RecetteC').value = machinery[i].value;
      break;
      case "Poids":
        display_Poids.setValue(machinery[i].value);
      break;
      case "Tare":
        display_Tare.setValue(machinery[i].value);
      break;
      case "Unite":
        document.getElementById('label_Poids').innerHTML = "Poids (" + machinery[i].value + ");";
      break;
      case "Etape":
        if(machinery[i].value == "PROCESSING") {
          $('.procesing_action').show();
          $('.finished_action').hide();
        } else {
          $('.procesing_action').hide();
          $('.finished_action').show();
        }
      break;
    }
  }
}

function switchInput(topic) {
  socket.emit('mqtt', JSON.stringify([{topic: RAMTopic + topic, payload : (document.getElementById('switch_' + topic).checked) ? "on" : "off", senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
}

function nivInput(topic) {
  socket.emit('mqtt', JSON.stringify([{topic: RAMTopic + topic, payload : document.getElementById('num_' + topic).value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
}

function changeMode() {
  if(document.getElementById('mode_select').value == "auto") {
    $('.autoMode').show();
    $('.manuMode').hide();
    socket.emit('mqtt', JSON.stringify([{topic : "RAM/balanceMel/cmd/Mode", payload : "auto", senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
    currentMode = 0;
  } else {
    $('.autoMode').hide();
    $('.manuMode').show();
    socket.emit('mqtt', JSON.stringify([{topic : "RAM/balanceMel/cmd/Mode", payload : "manuel", senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
    currentMode = 1;
  }
}

function mqttSend(topic, payload) {
  socket.emit('mqtt', JSON.stringify([{ topic : topic, payload : payload}]));
}

socket.on('RAM/balanceMel/etats/Poids', function(message) {
  display_Poids.setValue(message);
});

socket.on('RAM/balanceMel/etats/Tare', function(message) {
  display_Tare.setValue(message);
});

socket.on('RAM/balanceMel/etats/Unite', function(message) {
  document.getElementById('label_Poids').innerHTML = "Poids (" + message + ")";
});

socket.on('RAM/shopvac/etats/Etape', function(message) {
  if(message == "PROCESSING") {
    $('.procesing_action').show();
    $('.finished_action').hide();
  } else {
    $('.procesing_action').hide();
    $('.finished_action').show();
  }
});



$(document).ready(function() {
  $('.procesing_action').hide();
  changeMode();
  loadInfos(JSON.parse(document.getElementById('machineryStateParser').innerHTML));
});
