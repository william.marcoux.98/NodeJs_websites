var _MS_PER_DAY = 1000 * 60 * 60 * 24;
var dateMode = document.getElementById('dateMode_save_id').value || 0;

function dateDiffInDays(a, b) {
  alert('subtracting');

  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

var submitLogConstraints = function() {

	let order_select = document.getElementById('order_select');

	if(dateMode == 0) {
		let from_date_select = document.getElementById('from_date_select');
    document.getElementById('until_time_id').value = new Date().toISOString();
		document.getElementById('from_time_id').value = from_date_select.options[from_date_select.selectedIndex].value;
	} else {
    document.getElementById('until_time_id').value = document.getElementById('until_date_calendar').value;
		document.getElementById('from_time_id').value = (new Date() - new Date(document.getElementById('from_date_calendar').value));
	}

  document.getElementById('filter').value = document.getElementById('searchBar').value.toString();
	document.getElementById('order').value = order_select.options[order_select.selectedIndex].value;
	document.getElementById('dateMode_save_id').value = dateMode.toString();

  document.getElementById('constraintsForm').submit();
}

function doSearch(text, backgroundColor) {
    if (window.find && window.getSelection) {
        document.designMode = "on";
        var sel = window.getSelection();
        sel.collapse(document.body, 0);

        while (window.find(text)) {
            document.execCommand("HiliteColor", false, backgroundColor);
            sel.collapseToEnd();
        }
        document.designMode = "off";
    }
}
function paginate() {
  $('#logTable').after('<div id="pages" class="pagination"></div>');
    var rowsShown = 30;
    var rowsTotal = $('#logTable tbody tr').length;
    var numPages = rowsTotal/rowsShown;
      $('#pages').append('<a href="#" class="btn btn-default" rel="'+0+'"> «  </a> ');
    for(i = 0;i < numPages;i++) {
        var pageNum = i + 1;
        $('#pages').append('<a href="#" class="btn btn-default" rel="'+i+'">' + pageNum + '</a> ');
    }
    $('#pages').append('<a href="#" class="btn btn-default" rel="'+(numPages - 1)+'"> » </a> ');
    $('#logTable tbody tr').hide();
    $('#logTable tbody tr').slice(0, rowsShown).show();
    $('#pages a:first').addClass('active');
    $('#pages a').bind('click', function(){

        $('#pages a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#logTable tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).
        css('display','table-row').animate({opacity:1}, 300);
    });
}

$(document).ready(function() {
  if(dateMode == 0) {
    $(".date-calendar").hide();
  } else {
    $(".date-select").hide();
  }

  $('#searchBar').keydown(function(event){
   if(event.keyCode == 13) {
     event.preventDefault();
     submitLogConstraints();
     return false;
   }
 });

 //$('body').text().highlight(document.getElementById('searchBar').value);
 doSearch(document.getElementById('searchBar').value, "yellow");

paginate();

});

function toggleElements(class_1, class_2) {
  $("." + class_1).hide();
  $("." + class_2).show();
  dateMode = dateMode == 0 ? 1 : 0;
}
