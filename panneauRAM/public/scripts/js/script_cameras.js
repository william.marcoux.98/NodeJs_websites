var camera_1 = {
    IPv4 : "172.17.50.91"
};

var camera_2 = {
    IPv4 : "172.17.50.92"
};

var currentCamera;

$("#reloadBtn").click(function(){
  $("#cameraFrame").attr('src', "http://" + currentCamera.IPv4);
});

$("#switchBtn").click(function(){
    if(currentCamera.IPv4 == camera_1.IPv4) {
      currentCamera = camera_2;
    } else {
      currentCamera = camera_1;
    }

    $("#cameraFrame").attr('src', "http://" + currentCamera.IPv4);
});

$(document).ready(function(){
    currentCamera = camera_1;
    $("#cameraFrame").attr('src', "http://" + currentCamera.IPv4);
});
