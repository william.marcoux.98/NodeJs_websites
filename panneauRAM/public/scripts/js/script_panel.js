var gauge_NivGB = new LinearGauge({
    renderTo: 'gauge_NivGB',
    width: 200,
    height: 400,
    units: "%",
    minValue: 0,
    maxValue: 100,
    majorTicks: [
        "0",
        "10",
        "20",
        "30",
        "40",
        "50",
        "60",
        "70",
        "80",
        "90",
        "100"
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 95,
            "to": 100,
            "color": "rgba(200, 50, 50, .75)"
        }
    ],
    colorPlate: "#fff",
    borderShadowWidth: 0,
    borders: false,
    needleType: "arrow",
    needleWidth: 2,
    animationDuration: 1500,
    animationRule: "linear",
    tickSide: "left",
    numberSide: "left",
    needleSide: "left",
    barStrokeWidth: 7,
    barBeginCircle: false,
    value: 0
}).draw();

var gauge_NivPB = new LinearGauge({
    renderTo: 'gauge_NivPB',
    width: 200,
    height: 400,
    units: "%",
    minValue: 0,
    maxValue: 100,
    majorTicks: [
        "0",
        "10",
        "20",
        "30",
        "40",
        "50",
        "60",
        "70",
        "80",
        "90",
        "100"
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 95,
            "to": 100,
            "color": "rgba(200, 50, 50, .75)"
        }
    ],
    colorPlate: "#fff",
    borderShadowWidth: 0,
    borders: false,
    needleType: "arrow",
    needleWidth: 2,
    animationDuration: 1500,
    animationRule: "linear",
    tickSide: "left",
    numberSide: "left",
    needleSide: "left",
    barStrokeWidth: 7,
    barBeginCircle: false,
    value: 0
}).draw();

var gauge_TmpPB = new RadialGauge({
    renderTo: 'gauge_TmpPB',
    width: 300,
    height: 300,
    units: "°C",
    title: "",
    minValue: 0,
    maxValue: 100,
    majorTicks: [
        0,
        10,
        20,
        30,
        40,
        50,
        60,
        70,
        80,
        90,
        100
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 15,
            "color": "rgba(0,0, 255, .3)"
        },
        {
            "from": 85,
            "to": 100,
            "color": "rgba(255, 0, 0, .3)"
        }
    ],
    ticksAngle: 225,
    startAngle: 67.5,
    colorMajorTicks: "#ddd",
    colorMinorTicks: "#ddd",
    colorTitle: "#eee",
    colorUnits: "#ccc",
    colorNumbers: "#eee",
    colorPlate: "#222",
    borderShadowWidth: 0,
    borders: true,
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    needleCircleOuter: true,
    needleCircleInner: false,
    animationDuration: 1500,
    animationRule: "linear",
    colorBorderOuter: "#333",
    colorBorderOuterEnd: "#111",
    colorBorderMiddle: "#222",
    colorBorderMiddleEnd: "#111",
    colorBorderInner: "#111",
    colorBorderInnerEnd: "#333",
    colorNeedleShadowDown: "#333",
    colorNeedleCircleOuter: "#333",
    colorNeedleCircleOuterEnd: "#111",
    colorNeedleCircleInner: "#111",
    colorNeedleCircleInnerEnd: "#222",
    valueBoxBorderRadius: 0,
    colorValueBoxRect: "#222",
    colorValueBoxRectEnd: "#333",
    value: 0
}).draw();

var gauge_ValveGB = new RadialGauge({
    renderTo: 'gauge_ValveGB',
    width: 200,
    height: 200,
    units: "%",
    minValue: 0,
    maxValue: 100,
    majorTicks: [
        "0",
        "10",
        "20",
        "30",
        "40",
        "50",
        "60",
        "70",
        "80",
        "90",
        "100"
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 100,
            "color": "#fff"
        }
    ],
    colorPlate: "#fff",
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    animationDuration: 5,
    value: 0
}).draw();

var gauge_ValvePB = new RadialGauge({
    renderTo: 'gauge_ValvePB',
    width: 200,
    height: 200,
    units: "%",
    minValue: 0,
    maxValue: 100,
    majorTicks: [
        "0",
        "10",
        "20",
        "30",
        "40",
        "50",
        "60",
        "70",
        "80",
        "90",
        "100"
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 100,
            "color": "#fff"
        }
    ],
    colorPlate: "#fff",
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    animationDuration: 5,
    value: 0
}).draw();

var gauge_ValveEC = new RadialGauge({
    renderTo: 'gauge_ValveEC',
    width: 200,
    height: 200,
    units: "%",
    minValue: 0,
    maxValue: 100,
    majorTicks: [
        "0",
        "10",
        "20",
        "30",
        "40",
        "50",
        "60",
        "70",
        "80",
        "90",
        "100"
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 100,
            "color": "#fff"
        }
    ],
    colorPlate: "#fff",
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    animationDuration: 5,
    value: 0
}).draw();

var gauge_ValveEF = new RadialGauge({
    renderTo: 'gauge_ValveEF',
    width: 200,
    height: 200,
    units: "%",
    minValue: 0,
    maxValue: 100,
    majorTicks: [
        "0",
        "10",
        "20",
        "30",
        "40",
        "50",
        "60",
        "70",
        "80",
        "90",
        "100"
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 100,
            "color": "#fff"
        }
    ],
    colorPlate: "#fff",
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    animationDuration: 5,
    value: 0
}).draw();

function setDisplayAttributes(display) {
 display.pattern        = "###.####";
 display.displayAngle    = 6;
 display.digitHeight     = 20;
 display.digitWidth      = 14;
 display.digitDistance   = 2.5;
 display.segmentWidth    = 2;
 display.segmentDistance = 0.3;
 display.segmentCount    = 7;
 display.cornerType      = 3;
 display.colorOn         = "#ffffff";
 display.colorOff        = "#0c0c0c";
 display.setValue('000.0000');
 display.draw();
}

var display_Van = new SegmentDisplay('display_Van');
setDisplayAttributes(display_Van);
var display_Vbn = new SegmentDisplay('display_Vbn');
setDisplayAttributes(display_Vbn);
var display_Vab = new SegmentDisplay('display_Vab');
setDisplayAttributes(display_Vab);
var display_Ia = new SegmentDisplay('display_Ia');
setDisplayAttributes(display_Ia);
var display_Ib = new SegmentDisplay('display_Ib');
setDisplayAttributes(display_Ib);
var display_KW = new SegmentDisplay('display_KW');
setDisplayAttributes(display_KW);
var display_KWh = new SegmentDisplay('display_KWh');
setDisplayAttributes(display_KWh);
var display_FP = new SegmentDisplay('display_FP');
setDisplayAttributes(display_FP);

var currentMode;
var lastActionId = 0;
const RAMTopic = "RAM/panneau/cmd/";

function loadInfos(machinery) {
  for(let i = 0; i < machinery.length; i++) {
    switch(machinery[i].topic) {
      case "ConsNivGB":
        document.getElementById('num_ConsNivGB').value = machinery[i].value;
      break;
      case "ConsNivPB":
        document.getElementById('num_ConsNivPB').value = machinery[i].value;
      break;
      case "ConsTmpPB":
        document.getElementById('slider_ConsTmpPB').value = machinery[i].value;
        document.getElementById('display_ConsTmpPB_target').innerHTML = machinery[i].value.toString();
      break;
      case "ValveGB":
        gauge_ValveGB.value = machinery[i].value;
      break;
      case "ValvePB":
        gauge_ValvePB.value = machinery[i].value;
      break;
      case "ValveEC":
        gauge_ValveEC.value = machinery[i].value;
      break;
      case "ValveEF":
        gauge_ValveEF.value = machinery[i].value;
      break;
      case "NivGB":
        gauge_NivGB.value = machinery[i].value;
      break;
      case "NivPB":
        gauge_NivPB.value = machinery[i].value;
      break;
      case "TmpPB":
        gauge_TmpPB.value = machinery[i].value;
      break;
      case "ValveEEC":
        if(machinery[i].value.toLowerCase() == "on") {
          $('#display_ValveEEC').addClass('onZone').removeClass('offZone');
          $('#switch_ValveEEC').prop('checked', true);
          document.getElementById('zoneText_ValveEEC').innerHTML = "ON";
        } else if (machinery[i].value.toLowerCase() == "off") {
          $('#display_ValveEEC').addClass('offZone').removeClass('onZone');
          $('#switch_ValveEEC').prop('checked', false);
          document.getElementById('zoneText_ValveEEC').innerHTML = "OFF";
        }
      break;
      case "ValveEEF":
        if(machinery[i].value.toLowerCase() == "on") {
          $('#display_ValveEEF').addClass('onZone').removeClass('offZone');
          $('#switch_ValveEEF').prop('checked', true);
          document.getElementById('zoneText_ValveEEF').innerHTML = "ON";
        } else if (machinery[i].value.toLowerCase() == "off") {
          $('#display_ValveEEF').addClass('offZone').removeClass('onZone');
          $('#switch_ValveEEF').prop('checked', false);
          document.getElementById('zoneText_ValveEEF').innerHTML = "OFF";
        }
      break;
      case "Pompe":
        if(machinery[i].value.toLowerCase() == "on") {
          $('#switch_pompe').prop('checked', true);
        } else if (machinery[i].value.toLowerCase() == "off") {
          $('#switch_Pompe').prop('checked', false);
        }
      break;
      case "Van":
        display_Van.setValue(machinery[i].value);
      break;
      case "Vbn":
        display_Vbn.setValue(machinery[i].value);
      break;
      case "Vab":
        display_Vab.setValue(machinery[i].value);
      break;
      case "Ia":
        display_Ia.setValue(machinery[i].value);
      break;
      case "Ib":
        display_Ib.setValue(machinery[i].value);
      break;
      case "KW":
        display_KW.setValue(machinery[i].value);
      break;
      case "KWh":
        display_KWh.setValue(machinery[i].value);
      break;
      case "FP":
        display_FP.setValue(machinery[i].value);
      break;
    }
  }
}

function changeMode() {

  if(document.getElementById('mode_select').value == "auto") {
    $('.autoMode').show();
    $('.manuMode').hide();
    socket.emit('mqtt', JSON.stringify([{topic : RAMTopic + "Mode", payload : "auto", senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
    currentMode = 0;
  } else {
    $('.autoMode').hide();
    $('.manuMode').show();
    socket.emit('mqtt', JSON.stringify([{topic : RAMTopic + "Mode", payload : "manuel", senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
    currentMode = 1;
  }
  resetLastAction();
}

function resetLastAction() {
  lastActionId = 0;
}

function gaugeAction(buttonId, action) {

  if(currentMode == 0) return;
  lastActionId = buttonId;
  if(action == '' ) return;

  switch(buttonId) {
    case 1:
      (action == 'up') ? gauge_ValveGB.value++ : gauge_ValveGB.value--;
      socket.emit('mqtt', JSON.stringify([{topic : "RAM/panneau/cmd/ValveGB", payload : gauge_ValveGB.value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
    break;
    case 2:
      (action == 'up') ? gauge_ValvePB.value++ : gauge_ValvePB.value--;
      socket.emit('mqtt', JSON.stringify([{topic : "RAM/panneau/cmd/ValvePB", payload : gauge_ValvePB.value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
    break;
    case 3:
      (action == 'up') ? gauge_ValveEC.value++ : gauge_ValveEC.value--;
      socket.emit('mqtt', JSON.stringify([{topic : "RAM/panneau/cmd/ValveEC", payload : gauge_ValveEC.value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
    break;
    case 4:
      (action == 'up') ? gauge_ValveEF.value++ : gauge_ValveEF.value--;
      socket.emit('mqtt', JSON.stringify([{topic : "RAM/panneau/cmd/ValveEF", payload : gauge_ValveEF.value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
    break;
  }
}

function switchInput(topic) {
  socket.emit('mqtt', JSON.stringify([{topic: RAMTopic + topic, payload : (document.getElementById('switch_' + topic).checked) ? "on" : "off", senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
}

function nivInput(topic) {
  if(document.getElementById('num_' + topic).value.toString() == "") {return;}
  socket.emit('mqtt', JSON.stringify([{topic: RAMTopic + topic, payload : document.getElementById('num_' + topic).value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
}

function sliderInput(topic) {
  socket.emit('mqtt', JSON.stringify([{topic: RAMTopic + topic, payload : document.getElementById('slider_' + topic).value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
}

function sliderInputDisplay(topic, displayId) {
  document.getElementById(displayId).innerHTML = document.getElementById('slider_' + topic).value.toString();
}

socket.on('RAM/panneau/etats/NivGB', function(message){
  gauge_NivGB.update({value : parseFloat(message)});
});

socket.on('RAM/panneau/etats/NivPB', function(message){
  gauge_NivPB.update({value : parseFloat(message)});
});

socket.on('RAM/panneau/etats/NivGB', function(message){
  gauge_TmpPB.value = parseFloat(message);
});

socket.on('RAM/panneau/etats/TmpPB', function(message){
  gauge_TmpPB.value = parseFloat(message);
});

socket.on('RAM/panneau/etats/ValveGB', function(message){
  gauge_ValveGB.value = parseFloat(message);
});

socket.on('RAM/panneau/etats/ValvePB', function(message) {
  gauge_ValvePB.value = parseFloat(message);
});

socket.on('RAM/panneau/etats/ValveEC', function(message){
  gauge_ValveEC.value = parseFloat(message);
});

socket.on('RAM/panneau/etats/ValveEF', function(message){
  gauge_ValveEF.value = parseFloat(message);
});

socket.on('RAM/panneau/etats/ValveEEC', function(message){
  if(message.toLowerCase() == "on") {
    $('#display_ValveEEC').addClass('onZone').removeClass('offZone');
    document.getElementById('zoneText_ValveEEC').innerHTML = "ON";
  } else if (message.toLowerCase() == "off") {
    $('#display_ValveEEC').addClass('offZone').removeClass('onZone');
    document.getElementById('zoneText_ValveEEC').innerHTML = "OFF";
  }
});

socket.on('RAM/panneau/etats/ValveEEF', function(message) {
  if(message.toLowerCase() == "on") {
    $('#display_ValveEEF').addClass('onZone').removeClass('offZone');
    document.getElementById('zoneText_ValveEEF').innerHTML = "ON";
  } else if (message.toLowerCase() == "off") {
    $('#display_ValveEEF').addClass('offZone').removeClass('onZone');
    document.getElementById('zoneText_ValveEEF').innerHTML = "OFF";
  }
});

socket.on('RAM/panneau/etats/Pompe', function(message){
  if(message.toLowerCase() == "on") {
    $('#display_Pompe').addClass('onZone').removeClass('offZone');
    document.getElementById('zoneText_pompe').innerHTML = "ON";
  } else if (message.toLowerCase() == "off") {
    $('#display_Pompe').addClass('offZone').removeClass('onZone');
    document.getElementById('zoneText_pompe').innerHTML = "OFF";
  }
});

socket.on('RAM/powermeter/etats/Van', function(message) {
  display_Van.setValue(message);
});

socket.on('RAM/powermeter/etats/Vbn', function(message) {
  display_Vbn.setValue(message);
});

socket.on('RAM/powermeter/etats/Vab', function(message) {
  display_Vab.setValue(message);
});

socket.on('RAM/powermeter/etats/Ia', function(message) {
  display_Ia.setValue(message);
});

socket.on('RAM/powermeter/etats/Ib', function(message) {
  display_Ib.setValue(message);
});

socket.on('RAM/powermeter/etats/KW', function(message) {
  display_KW.setValue(message);
});

socket.on('RAM/powermeter/etats/KWh', function(message) {
  display_KWh.setValue(message);
});

socket.on('RAM/powermeter/etats/FP', function(message) {
  display_FP.setValue(message);
});

$("#num_NivGB").keyup(function() {
  nivInput('NivGB');
});

$("#num_NivPB").keyup(function() {
  nivInput('NivPB');
});

document.onkeydown = function (key) {
  if(key.keyCode == 40) {
    switch(lastActionId) {
      case 1:
        key.preventDefault();
        gauge_ValveGB.value-- ;
      break;
      case 2:
        key.preventDefault();
        gauge_ValvePB.value--;
      break;
      case 3:
        key.preventDefault();
        gauge_ValveEC.value--;
      break;
      case 4:
        key.preventDefault();
        gauge_ValveEF.value--;
      break;
      default:

      break;
    }
  } else if (key.keyCode == 38) {
    switch(lastActionId) {
      case 1:
        key.preventDefault();
        gauge_ValveGB.value++;
      break;
      case 2:
        key.preventDefault();
        gauge_ValvePB.value++;
      break;
      case 3:
        key.preventDefault();
        gauge_ValveEC.value++;
      break;
      case 4:
        key.preventDefault();
        gauge_ValveEF.value++;
      break;
      default:

      break;
    }
  }
};

document.onkeyup = function(key) {
    if((key.keyCode == 40 || key.keyCode == 38) && lastActionId != 0) {
      switch(lastActionId) {
        case 1:
          socket.emit('mqtt', JSON.stringify([{topic : "RAM/panneau/cmd/ValveGB", payload : gauge_ValveGB.value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
        break;
        case 2:
          socket.emit('mqtt', JSON.stringify([{topic : "RAM/panneau/cmd/ValvePB", payload : gauge_ValvePB.value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
        break;
        case 3:
          socket.emit('mqtt', JSON.stringify([{topic : "RAM/panneau/cmd/ValveEC", payload : gauge_ValveEC.value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
        break;
        case 4:
          socket.emit('mqtt', JSON.stringify([{topic : "RAM/panneau/cmd/ValveEF", payload : gauge_ValveEF.value.toString(), senderUser_id : userInfo.user_id, senderUser_username: userInfo.user_username}]));
        break;
        default:

        break;
      }
    }
};

$(document).ready(function() {

  changeMode();
  loadInfos(JSON.parse(document.getElementById('machineryStateParser').innerHTML));
  document.getElementById('display_TempPB_target').innerHTML = '0';
});
